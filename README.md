# HZDR GitLab

## Recent changes introduced in the GitLab Core Edition

GitLab releases new versions including new features every 22nd of each month. GitLab@HZDR usually runs
the most recent release with the first `*.1` release a few days after the 22nd. The release blog posts of
the three most recent releases are collected below:

- GitLab 13.8: https://about.gitlab.com/releases/2021/01/22/gitlab-13-8-released/
- GitLab 13.7: https://about.gitlab.com/releases/2020/12/22/gitlab-13-7-released/
- GitLab 13.6: https://about.gitlab.com/releases/2020/11/22/gitlab-13-6-released/

## Issue Tracker for gitlab.hzdr.de

Please file an issue if you wish to submit a feature request, want to report 
a bug or need help. ([Issues](https://gitlab.hzdr.de/fwcc/HZDR-GitLab/issues))

## Tips and tricks

In the Wiki of this repository we will host a collection of tips, tricks and
best practices for using Git and Gitlab: 
[Wiki](https://gitlab.hzdr.de/fwcc/HZDR-GitLab/wikis)

## Documentation

The official GitLab-Documentation can be found here: https://gitlab.hzdr.de/help

## Backup Strategy

Every night a full backup of the HZDR GitLab is made. The backup is
suited for restoring the whole instance in case of failure. Single projects
(except the Git repository itself) cannot be extracted from the backup. Registry
images are not included in the backup, as they can be regenareted.

Backups are moved to a different location and stored on tape.

## Examples

- https://gitlab.com/gitlab-examples
- https://gitlab.hzdr.de/examples

## Slides

- https://gitlab-org.gitlab.io/end-user-training-slides
- https://gitlab-org.gitlab.io/eclipse-gitlab-slides
- https://gitlab-org.gitlab.io/ci-training-slides
